package ito.poo.clases;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class Composicion {

	private String titutlo;
	private  LocalTime duracion;
	private String generoM;
	private ArrayList<String>interpretes;
	private LocalDate fecharegistro;
	private LocalDate fechaestreno;
	
	public boolean addInter(String interprete) {
		boolean intr=false;
		if(!interpretes.contains(interprete)) {
			interpretes.add(interprete);
			intr=true;
		}
		return intr;
	}
	public Composicion(String titutlo, LocalTime duracion, String generoM) {
		super();
		this.titutlo = titutlo;
		this.duracion = duracion;
		this.generoM = generoM;
		this.interpretes= new ArrayList<String>();
	}
	public LocalDate getFecharegistro() {
		return fecharegistro;
	}
	public void setFecharegistro(LocalDate fecharegistro) {
		this.fecharegistro = fecharegistro;
	}
	public LocalDate getFechaestreno() {
		return fechaestreno;
	}
	public void setFechaestreno(LocalDate fechaestreno) {
		this.fechaestreno = fechaestreno;
	}
	public String getTitutlo() {
		return titutlo;
	}
	public LocalTime getDuracion() {
		return duracion;
	}
	public String getGeneroM() {
		return generoM;
	}
	public ArrayList<String> getInterpretes() {
		return interpretes;
	}
	@Override
	public String toString() {
		return "Composicion [titutlo=" + titutlo + ", duracion=" + duracion + ", generoM=" + generoM + ", interpretes="
				+ interpretes + ", fecharegistro=" + fecharegistro + ", fechaestreno=" + fechaestreno + "]";
	}
}
